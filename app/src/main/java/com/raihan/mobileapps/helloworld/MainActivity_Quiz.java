package com.raihan.mobileapps.helloworld;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity_Quiz extends AppCompatActivity {

    EditText ed1;
    TextView tv1,tv2,tv3;
    RadioButton a,b,c,d;
    Button bt;
    RadioGroup rg;
    int q,s;
    ImageView emotSenyum, emotSedih, emotMarah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_quiz);
        ed1=(EditText) findViewById(R.id.name);
        tv1=(TextView)findViewById(R.id.ques);
        tv2=(TextView)findViewById(R.id.response);
        tv3=(TextView)findViewById(R.id.score);
        rg=(RadioGroup)findViewById(R.id.optionGroup);
        a=(RadioButton)findViewById(R.id.option1);
        b=(RadioButton)findViewById(R.id.option2);
        c=(RadioButton)findViewById(R.id.option3);
        d=(RadioButton)findViewById(R.id.option4);
        bt=(Button)findViewById(R.id.next);
        emotSenyum = (ImageView)findViewById(R.id.emotSenyum);
        emotSedih = (ImageView)findViewById(R.id.emotSedih);
        emotMarah = (ImageView)findViewById(R.id.emotMarah);
        q=0;
        s=0;

    }
    public void quiz(View v){
        switch (q) {
            case 0: {
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                tv2.setText("");
                tv3.setText("");
                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(true);
                bt.setText("Next");
                s = 0;

                tv1.setText("1. 5 + 5 = ?");
                a.setText("15");
                b.setText("10");
                c.setText("25");
                d.setText("20");
                q = 1;
                break;
            }
            case 1: {
                ed1.setEnabled(false);
                tv1.setText("2. 15 + 15 = ?");
                a.setText("25");
                b.setText("30");
                c.setText("35");
                d.setText("40");

                if (b.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    jawabanSalah();
                    s = 0;
                } else {
                    belumMenjawab();
                    s = s - 10;
                }

                q = 2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 2: {
                tv1.setText("3. 10 + 5 = ?");
                a.setText("20");
                b.setText("10");
                c.setText("15");
                d.setText("5");

                if (b.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }

                q = 3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 3: {
                tv1.setText("4. 100 : 5 = ?");
                a.setText("25");
                b.setText("30");
                c.setText("15");
                d.setText("20");

                if (c.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }

                q = 4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 4: {
                tv1.setText("5. 4 x 5 = ?");
                a.setText("20");
                b.setText("30");
                c.setText("40");
                d.setText("50");
                if (d.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }
                q = 5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 5: {
                tv1.setText("6. 50 - 20 = ?");
                a.setText("70");
                b.setText("60");
                c.setText("30");
                d.setText("40");
                if (a.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }
                q = 6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 6: {
                tv1.setText("7. 10 x 7 = ?");
                a.setText("70");
                b.setText("60");
                c.setText("50");
                d.setText("40");
                if (c.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }
                q = 7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 7: {
                tv1.setText("8. 100 : 2 = ?");
                a.setText("20");
                b.setText("30");
                c.setText("40");
                d.setText("50");
                if (a.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }
                q = 8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 8: {
                tv1.setText("9. 10 - 9 = ?");
                a.setText("4");
                b.setText("3");
                c.setText("2");
                d.setText("1");
                if (d.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }
                q = 9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 9: {
                tv1.setText("10. 200 - 50 = ?");
                a.setText("100");
                b.setText("120");
                c.setText("150");
                d.setText("90");
                if (d.isChecked()) {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }
                q=10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt.setText("Finish");
                break;
            }
            case 10: {
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                if (c.isChecked())
                {
                    jawabanBenar();
                    s = s + 50;
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    belumMenjawab();
                    s = 0;
                } else {
                    jawabanSalah();
                    s = s - 10;
                }
                tv3.setText(ed1.getText()+"'s final score is "+s);
                bt.setText("Restart");
                q=0;
                break;
            }
        }
    }
    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void jawabanBenar()
    {
        tv2.setText("Selamat Anda benar, Anda mendapatkan 50 poin.");
        s=s+50;
        emotSenyum.setVisibility(View.VISIBLE);
        emotSenyum.postDelayed(new Runnable() {
            @Override
            public void run() {
                emotSenyum.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void belumMenjawab()
    {
        tv2.setText("Kamu belum menjawab dan memilih jawaban !!!");
        s=s+0;
        emotMarah.setVisibility(View.VISIBLE);
        emotMarah.postDelayed(new Runnable() {
            @Override
            public void run() {
                emotMarah.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void jawabanSalah()
    {
        tv2.setText("Jawaban kamu salah\nD adalah jawaban yang benar.");
        s=s-10;
        emotSedih.setVisibility(View.VISIBLE);
        emotSedih.postDelayed(new Runnable() {
            @Override
            public void run() {
                emotSedih.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }
}